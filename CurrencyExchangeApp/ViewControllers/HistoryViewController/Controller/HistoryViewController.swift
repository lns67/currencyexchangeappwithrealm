//
//  HistoryViewController.swift
//  CurrencyExchangeApp
//
//  Created by DS on 07.11.2021.
//

import UIKit

// MARK: - HistoryViewController
class HistoryViewController: UIViewController {

    @IBOutlet weak var historyTableView: UITableView!
    var exchangesHistory: [CurrencyManager] = []
    let historyCellID = "HistoryTableViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        historyTableView.register(UINib(nibName: historyCellID, bundle: nil), forCellReuseIdentifier: historyCellID)
        historyTableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        DatabaseManager.shared.getData { list in
            self.exchangesHistory = list?.currenciesArray ?? []
            DispatchQueue.main.async {
                self.historyTableView.reloadData()
            }
        }
    }
    
    @IBAction func onBackArrow(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

//
//  HistoryTableViewCell.swift
//  CurrencyExchangeApp
//
//  Created by DS on 07.11.2021.
//

import UIKit

// MARK: - HistoryTableViewCell
class HistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var currencyInfoLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func update(currencyManager: CurrencyManager) {
        currencyInfoLabel.text = "\(String(currencyManager.enteredAmount)) - \(currencyManager.fromCurrency)\n\(String(currencyManager.result)) - \(currencyManager.toCurrency)"
    }
}

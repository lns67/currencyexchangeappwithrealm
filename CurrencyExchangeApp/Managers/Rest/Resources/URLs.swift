//
//  URLs.swift
//  CurrencyExchangeApp
//
//  Created by DS on 01.11.2021.
//

import Foundation

// MARK: - URLs
struct URLs {
    static let listCurrenciesExchange = "https://currency-exchange.p.rapidapi.com/listquotes"
    static let currencyExchange = "https://currency-exchange.p.rapidapi.com/exchange"
}

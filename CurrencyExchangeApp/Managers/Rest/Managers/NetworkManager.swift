//
//  NetworkManager.swift
//  CurrencyExchangeApp
//
//  Created by DS on 01.11.2021.
//

import Foundation
import Alamofire

// MARK: - NetworkManager
class NetworkManager {
    
    static let shared = NetworkManager()
    
    let apiHost = "currency-exchange.p.rapidapi.com"
    let apiKey = "078330417bmsh94a5cd2f513d694p1616e8jsn59bf695086c9"
    
    func networkRequest(parametrs: [String : Any]? = nil,
                        method: HTTPMethod,
                        headers: [String : String]? = nil,
                        urtlPath: String,
                        completionHandler: @escaping (Data?, String?) -> Void) {
        
        var staticHTTPHeaders: [String : String] = ["X-RapidAPI-Host" : apiHost , "X-RapidAPI-Key" : apiKey]
        
        var httpHeaders: [HTTPHeader] = []
        if let headers = headers {
            for (key, value) in headers {
                staticHTTPHeaders[key] = value
            }
        }
        
        staticHTTPHeaders.forEach { (key, value) in
            let httpHeader = HTTPHeader(name: key, value: value)
            httpHeaders.append(httpHeader)
        }
        
        AF.request(urtlPath,
                   method: .get,
                   parameters: parametrs,
                   headers: HTTPHeaders(httpHeaders)).response { response in
            
            switch response.result {
                
            case .success(let data):
                completionHandler(data, nil)
            case .failure(let error):
                completionHandler(nil, error.localizedDescription)
            }
        }
    }
}

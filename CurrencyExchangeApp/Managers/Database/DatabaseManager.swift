//
//  DatabaseManager.swift
//  CurrencyExchangeApp
//
//  Created by DS on 07.11.2021.
//

import Foundation
import RealmSwift

class DatabaseManager {
    
    static let shared = DatabaseManager()
    private let realm = try? Realm()
    
    func saveData(currency: CurrencyManager) {
        
        let databaseCurrency = transformDatabaseElement(currency: currency)
        
        do {
            
            try realm?.write {
                realm?.add(databaseCurrency)
            }
        } catch(let error) {
            print(error.localizedDescription)
        }
    }
    
    func getData(completionHadler: @escaping(ListOfCurrenciesExchanges?) -> Void) {

        guard let databaseListOfCurrencies = realm?.objects(DatabaseCurrency.self) else {

            completionHadler(nil)
            return
        }

        completionHadler(transformElement(databaseListOfCurrencies: databaseListOfCurrencies))
    }
    
    func deleteData(currency: CurrencyManager) {
        
        let databaseCurrency = transformDatabaseElement(currency: currency)
        
        do {
            
            let object = realm?.objects(DatabaseCurrency.self).filter({$0 == databaseCurrency}).first
            
            try! realm?.write {
                if let object = object {
                    realm?.delete(object)
                }
            }
        } catch(let error) {
            print(error.localizedDescription)
        }
    }
    
    // MARK: - transform element from usual to Database
    private func transformDatabaseElement(currency: CurrencyManager) -> DatabaseCurrency {

        let databaseCurrency: DatabaseCurrency = DatabaseCurrency()

        databaseCurrency.fromCurrency = currency.fromCurrency
        databaseCurrency.toCurrency = currency.toCurrency
        databaseCurrency.enteredAmount = currency.enteredAmount
        databaseCurrency.result = currency.result

        return databaseCurrency
    }

    // MARK: - transform element from Database to usual
    private func transformElement(databaseListOfCurrencies: Results<DatabaseCurrency>) -> ListOfCurrenciesExchanges {
        var currenciesArray: [CurrencyManager] = []
        
        for databaseCurrency in databaseListOfCurrencies {
            
            if let fromCurrency = databaseCurrency.fromCurrency, let toCurrency = databaseCurrency.toCurrency {
                
                let currencyManager = CurrencyManager(fromCurrency: fromCurrency, toCurrency: toCurrency, enteredAmount: databaseCurrency.enteredAmount, result: databaseCurrency.result)
                
                currenciesArray.append(currencyManager)
            }
        }
        
        let listOfCurrenciesExchanges: ListOfCurrenciesExchanges = ListOfCurrenciesExchanges(currenciesArray: currenciesArray)
                
        return listOfCurrenciesExchanges
    }
}

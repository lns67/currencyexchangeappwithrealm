//
//  DatabaseCurrency.swift
//  CurrencyExchangeApp
//
//  Created by DS on 07.11.2021.
//

import Foundation
import Realm
import RealmSwift

class DatabaseListOfCurrencies: Object {
    var response: List<DatabaseCurrency> = List<DatabaseCurrency>()
}

class DatabaseCurrency: Object {
    @objc dynamic var fromCurrency: String?
    @objc dynamic var toCurrency: String?
    @objc dynamic var enteredAmount: Double = 0
    @objc dynamic var result: Double = 0
    
    static func == (databaseCurrency1: DatabaseCurrency, databaseCurrency2: DatabaseCurrency) -> Bool {
        return (databaseCurrency1.fromCurrency == databaseCurrency2.fromCurrency &&
                databaseCurrency1.toCurrency == databaseCurrency2.toCurrency &&
                databaseCurrency1.enteredAmount == databaseCurrency2.enteredAmount &&
                databaseCurrency1.result == databaseCurrency2.result)
    }
}

//
//  CurrencyManager.swift
//  CurrencyExchangeApp
//
//  Created by DS on 07.11.2021.
//

import Foundation

struct ListOfCurrenciesExchanges {
    var currenciesArray: [CurrencyManager]
}

struct CurrencyManager {
    var fromCurrency: String
    var toCurrency: String
    var enteredAmount: Double
    var result: Double
}
